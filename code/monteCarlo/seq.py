# Code accompanying the paper: "Inverse design of viral infectivity enhancing peptides fibrils from continuous protein-vector embeddings" by K. Kaygisiz, A. Dutta, L. Rauch-Wirth, C. V. Synatschke, J. Münch, T. Bereau, and T. Weil

import numpy as np
import pandas as pd
import random
import pickle
import warnings
warnings.filterwarnings('ignore')
# ==============================================================================
# Relevant external data
threeGrams = pd.read_csv(
    '../../data/raw/dataverse_files/protVec_100d_3grams.csv', sep='\t')

allAA = [
    'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R',
    'S', 'T', 'V', 'W', 'Y'
]

# List of names for descriptors

protVecs = ['vec' + str(x) for x in range(1, 101)]

# Import the trained LASSO model

with open('lasso_model_protVecs_log10rel_infectivity.pkl', 'rb') as file:
    lasso_model_protVecs_log10rel_infectivity = pickle.load(file)
# ==============================================================================

def create_protVec_df_from_seq(seqs):
    """
    Takes a list of sequences and returns a dataframe with calculated protVecs
    """

    df = pd.DataFrame(columns=['sequence'])
    df['sequence'] = seqs
#     print(df)

    seq_vecs = np.zeros((len(seqs), 100))
    trips = []
    for j, s in enumerate(seqs):
        seq_vec = np.zeros(100)
    #     print(j,s)
        for i in range(len(s)-2):
            trip = s[i]+s[i+1]+s[i+2]
            trips.append(trip)
            seq_vec = seq_vec + \
                np.array(
                    threeGrams.loc[threeGrams['words'] == trip].T[1:]).T[0]
        seq_vecs[j][:] = seq_vec
    #     break
    # print("ProtVec import:", seq_vecs.shape)

    protVec = pd.concat([df, pd.DataFrame(seq_vecs)], axis=1)

    # Rename the columns. They are integers in the csv, change them like 0->vec0

    def conv(x):
        if (isinstance(x, str) != True):
            return 'vec' + str(int(x)+1)
        else:
            return x
    protVec.columns = protVec.columns.map(lambda x: conv(x))
    return protVec


# print(create_protVec_df_from_seq(['DMRYTN']))
# ==============================================================================


def gen_peptide_using_MC(startSeq):
    """
    Takes a starting AA sequence. It then flips one AA, measures the change in log10rel_infectivity using the LASSO protVec regressions, and accepts the new sequence according to the Metropolis algorithm (see paper for details).
    """
    pepLength = len(startSeq)

    # Pick a random position in the startSeq.
    position = random.randint(0, pepLength - 1)
    # print(position, startSeq, startSeq[:position], startSeq[position],
    # startSeq[position+1:])

    # choose a random AA
    AA = random.choice(allAA)

    # Create a new peptide by changing the AA in the startSeq
    newSeq = startSeq[:position] + AA + startSeq[position + 1:]
    # print(AA, startSeq, newSeq)

    # Calculate log10rel_infectivity
    old_log10rel_infectivity_from_protVecs_lasso = lasso_model_protVecs_log10rel_infectivity.predict(
        create_protVec_df_from_seq([startSeq]).loc[:, protVecs])
    new_log10rel_infectivity_from_protVecs_lasso = lasso_model_protVecs_log10rel_infectivity.predict(
        create_protVec_df_from_seq([newSeq]).loc[:, protVecs])

    # MC move
    accept = False
    delta_log10rel_infectivity = new_log10rel_infectivity_from_protVecs_lasso - \
        old_log10rel_infectivity_from_protVecs_lasso
    x = np.exp(delta_log10rel_infectivity)
    # print(delta_log10rel_infectivity, x, startSeq, newSeq)

    if (delta_log10rel_infectivity >= 0):
        accept = True
    else:
        if (x >= random.uniform(0, 1)):
            accept = True
        else:
            accept = False
    if (accept):
        return newSeq
    else:
        # print(f'Move rejected: {delta_log10rel_infectivity}, {x}, {startSeq}, {newSeq}')
        return startSeq
# ==============================================================================


def run_singleSeed_MC(startSeq, MC_steps):
    """
    Core MC loop that starts from a single sequence, updates it according to MC
    rule, and returns the list of sequences that are accepted by the MC steps.
    """
    generated_peptides = [startSeq]
    for i in range(MC_steps):
        newSeq = gen_peptide_using_MC(startSeq)
#        print(startSeq,newSeq)
        if (newSeq == startSeq):
            pass
        else:
            generated_peptides.append(newSeq)
            startSeq = newSeq
    return generated_peptides


def get_startSeqs(pep_len, num_startSeqs):
    return [''.join(random.sample(allAA, pep_len))
            for i in range(num_startSeqs)]


if __name__ == '__main__':
    print(run_singleSeed_MC('KIKIQI', 100))
