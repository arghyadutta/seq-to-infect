from seq import run_singleSeed_MC, get_startSeqs
import pandas as pd

num_startSeqs = 2
pep_len = 6

# Note I set the MC_steps to 100 and save the generated csv to generated_seqs/trial folder to avoid overwriting the generated sequences that were used in this work. Also, we used 10**6 steps for the results shown in the paper.

MC_steps = 10**2
startSeqs = get_startSeqs(pep_len, num_startSeqs)
# print(startSeqs)

for seq in startSeqs:
    df = pd.DataFrame()
    df[0] = run_singleSeed_MC(seq, MC_steps)
    # print(df)
    df.to_csv(
        f'./generated_seqs/trial/{seq}_{df.shape[0]}.csv', sep=',', header=False, index=False)
