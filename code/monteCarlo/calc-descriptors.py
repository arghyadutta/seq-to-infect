# Code used in the paper: "Inverse design of viral infectivity enhancing peptides fibrils from continuous protein-vector embeddings" by K. Kaygisiz, A. Dutta, L. Rauch-Wirth, C. V. Synatschke, J. Münch, T. Bereau, and T. Weil

import numpy as np
import pandas as pd
import pickle
import sys
from pathlib import Path
from timeit import default_timer as timer
import warnings
warnings.filterwarnings('ignore')
# ==============================================================================
# Relevant external data
threeGrams = pd.read_csv(
    '../../data/raw/dataverse_files/protVec_100d_3grams.csv', sep='\t')

protVecs = ['vec' + str(x) for x in range(1, 101)]

# ==============================================================================
# Import trained LASSO models from pickle files

with open('lasso_model_protVecs_log10rel_infectivity.pkl', 'rb') as file:
    lasso_model_protVecs_log10rel_infectivity = pickle.load(file)
with open('lasso_model_protVecs_zeta.pkl', 'rb') as file:
    lasso_model_protVecs_zeta = pickle.load(file)
# ==============================================================================


def create_protVec_df_from_seq(df):
    """
    Returns a dataframe with calculated protVecs
    """
    seqs = df['sequence']
    seq_vecs = np.zeros((len(seqs), 100))
    trips = []
    for j, s in enumerate(seqs):
        seq_vec = np.zeros(100)
    #     print(j,s)
        for i in range(len(s)-2):
            trip = s[i]+s[i+1]+s[i+2]
            trips.append(trip)
            seq_vec = seq_vec + \
                np.array(
                    threeGrams.loc[threeGrams['words'] == trip].T[1:]).T[0]
        seq_vecs[j][:] = seq_vec
    #     break
    # print("ProtVec import:", seq_vecs.shape)

    def conv(x):
        if (isinstance(x, str) != True):
            return 'vec' + str(int(x)+1)
        else:
            return x
    protVecs = pd.DataFrame(seq_vecs)
    protVecs.columns = protVecs.columns.map(lambda x: conv(x))
    # print(protVecs)
    protVecs = pd.concat([df, protVecs], axis=1)

    return protVecs


def calc_protVec_descs(df):
    data_protVec = create_protVec_df_from_seq(df)
    data_protVec['zeta_from_protVecs_lasso'] = lasso_model_protVecs_zeta.predict(
        data_protVec.loc[:, protVecs])
    data_protVec['log10rel_infectivity_from_protVecs_lasso'] = lasso_model_protVecs_log10rel_infectivity.predict(
        data_protVec.loc[:, protVecs])
    # We don't need the raw protVecs
    data_protVec.drop(protVecs, axis=1, inplace=True)

    # NOTE: uncomment the next line for production runs. I commented it to avoid overwriting the generated sequence files that were used during tests
    # data_protVec.to_csv(
    #     f'./generated_seqs/with_fauchere_protVec/{sys.argv[1]}.csv', index=False)
    # print(data_protVec)
    return data_protVec


# Run the code as `python calc-descriptors.py ASWKLY_860331`. ASWKLY_860331 is the name of a csv file (path: code/monteCarlo/generated_seqs/with_fauchere/ASWKLY_860331.csv) with sequence and hydrophobicity.

filePath = Path(f'./generated_seqs/with_fauchere/{sys.argv[1]}.csv')

# Check for a small batch?
nSample = 5
data = pd.read_csv(filePath)[:nSample].reset_index(drop=True)
print("Input")
print(data)
print('----------------------------')
print('Input sequences with 100D protVecs')
print(create_protVec_df_from_seq(data))
print('----------------------------')
print('Input sequences with predicted descriptors')
print(calc_protVec_descs(data))

# Uncomment the following for full run.

# data = pd.read_csv(filePath)
# start = timer()
# calc_protVec_descs(data).to_csv(f'./generated_seqs/with_fauchere_protVec/{sys.argv[1]}.csv', index=False)
# end = timer()
# print(f'time taken for {nSample} peptides = {end-start} s')
